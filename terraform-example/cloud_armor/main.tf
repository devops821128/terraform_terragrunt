resource "random_id" "suffix" {
  byte_length = 4
}

module "cloud_armor" {
  source  = "GoogleCloudPlatform/cloud-armor/google"
  version = "~> 2.0"

  project_id                           = var.project_id
  name                                 = "cloudflare-white-list"
  description                          = "Cloudflare white list IP & Home IP"
  default_rule_action                  = "deny(403)"
  type                                 = "CLOUD_ARMOR"
  layer_7_ddos_defense_enable          = true
  layer_7_ddos_defense_rule_visibility = "STANDARD"
  user_ip_request_headers              = []

  security_rules = {
    "cloudflare_ip0" = {
      action        = "allow"
      priority      = 0
      description   = "Cloudflare IP"
      src_ip_ranges = ["173.245.48.0/20", "103.21.244.0/22", "103.22.200.0/22", "103.31.4.0/22", "141.101.64.0/18", "108.162.192.0/18", "190.93.240.0/20", "188.114.96.0/20", "197.234.240.0/22", "198.41.128.0/17",]
      preview       = false
    }

    "cloudflare_ip1" = {
      action        = "allow"
      priority      = 1
      description   = "Cloudflare IP"
      src_ip_ranges = ["162.158.0.0/15", "104.16.0.0/12", "172.64.0.0/13", "131.0.72.0/22",]
      preview       = false
    }

    "home_ip" = {
      action        = "allow"
      priority      = 2
      description   = "20F Office IP"
      src_ip_ranges = ["220.130.64.247",]
      preview       = false
    }
  }
}

variable "project_id" {
  description = "The ID of the project in which resources will be provisioned."
  type        = string
}

variable "serviceAccount" {
  description = "GCP Project Service Account."
  type        = string
}

variable "owasp_rules" {
  type = map(object({
    priority      = string
    src_ip_ranges = list(string)
    description   = string
    preview_mode  = bool
    action        = string
  }))
}

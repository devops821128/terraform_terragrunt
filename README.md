# Terraform-Resource

### 環境與專案ID對應表
| ProjectID            | Environment         |
| -------------------- |:------------------: |
|  | Dev                 |
|  | Qa              |
|  | Integration-Test             |
|  | Staging             |
|  | Production      |

terraform {
  source = "../modules/cloud_armor"
}

inputs = {
  project_id     = ""
  region         = ""
  serviceAccount = "terraform_sa.json"
}

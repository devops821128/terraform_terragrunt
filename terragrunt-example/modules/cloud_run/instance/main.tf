module "cloud_run" {
  source  = "GoogleCloudPlatform/cloud-run/google"
  version = "~> 0.10"

  for_each              = toset(var.cloud_run_name)
  service_name          = each.value
  project_id            = var.project_id
  location              = var.region
  image                 = "us-docker.pkg.dev/cloudrun/container/hello"
  service_account_email = var.cloud_run_sa

  template_annotations = {
    "autoscaling.knative.dev/maxScale"        = 10
    "autoscaling.knative.dev/minScale"        = 1
    "run.googleapis.com/vpc-access-connector" = var.vpc_connector
    "run.googleapis.com/vpc-access-egress"    = "all-traffic"
  }
}

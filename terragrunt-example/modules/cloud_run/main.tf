# modules
module "instance" {
  source         = "./instance"
  project_id     = var.project_id
  region         = var.region
  cloud_run_name = var.cloud_run_name
  serviceAccount = var.serviceAccount
  vpc_connector  = var.vpc_connector
  cloud_run_sa   = var.cloud_run_sa
}

module "role" {
  source         = "./role"
  project_id     = var.project_id
  region         = var.region
  cloud_run_name = var.cloud_run_name
  serviceAccount = var.serviceAccount
}

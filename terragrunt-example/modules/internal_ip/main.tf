resource "google_compute_address" "internal-ip" {
  name         = "internal-ip"
  subnetwork   = var.subnetwork
  address_type = "INTERNAL"
  region       = var.region
}
